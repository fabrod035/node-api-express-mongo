const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
    res.status(200).json({success : true, msg: 'Show all test'})
})

router.get('/:id', (req, res) => {
    res.status(200).json({success : true, msg: `Get test ${req.params.id}`})
})

router.post('/', (req, res) => {
    res.status(200).json({success : true, msg: 'Create new test'})
})

router.put('/:id', (req, res) => {
    res.status(200).json({success : true, msg: `Update test ${req.params.id}`})
})

router.delete('/:id', (req, res) => {
    res.status(200).json({success : true, msg: `Delete test ${req.params.id}`})
})

module.exports = router;